/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : swoft_test

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 04/05/2023 16:46:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `add_time` int DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', 1683184154);
INSERT INTO `user` VALUES (2, 'lisi', 'e10adc3949ba59abbe56e057f20f883e', 0);
INSERT INTO `user` VALUES (5, 'wangwu', 'e10adc3949ba59abbe56e057f20f883e', 0);
COMMIT;

-- ----------------------------
-- Table structure for user_count
-- ----------------------------
DROP TABLE IF EXISTS `user_count`;
CREATE TABLE `user_count` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT '0',
  `follow` int DEFAULT '0',
  `fans` int DEFAULT '0',
  `order_count` int DEFAULT '0',
  `add_time` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user_count
-- ----------------------------
BEGIN;
INSERT INTO `user_count` VALUES (1, 1, 5, 10, 8, 0);
INSERT INTO `user_count` VALUES (2, 2, 9, 4, 22, 0);
INSERT INTO `user_count` VALUES (3, 5, 19, 21, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT '0' COMMENT '用户ID',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `sex` tinyint DEFAULT '0' COMMENT '性别0=保密 1=男 2=女',
  `introduction` varchar(255) DEFAULT NULL COMMENT '简介',
  `homepage` varchar(100) DEFAULT NULL COMMENT '个人主页',
  `add_time` int DEFAULT '0' COMMENT '添加时间',
  `update_time` int DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info` VALUES (1, 1, '奥特曼', NULL, 0, '我是一个超人', 'www.abc.com', 1683184154, 1683184768);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
