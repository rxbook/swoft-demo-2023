<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

function user_func(): string
{
    return 'hello';
}

/**
 * 成功的返回
 * @param array $data
 * @param int $code
 * @param string $msg
 * @return \Swoft\Http\Message\Response|static
 */
function returnSuccess($data = [], $code = 0, $msg = 'success'){
    $result = [
        'code' => $code,
        'msg' => $msg,
        'data' => $data
    ];
    return context()->getResponse()->withStatus(200)->withData($result);
}

/**
 * 失败的返回
 * @param int $code
 * @param string $msg
 * @return \Swoft\Http\Message\Response|static
 */
function returnError($code = -1, $msg = 'error'){
    $code = ($code == 0)?-1:$code;
    $result = [
        'code' => $code,
        'msg' => $msg
    ];
    return context()->getResponse()->withStatus(200)->withData($result);
}

