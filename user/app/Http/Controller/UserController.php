<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller;

use Exception;
use Swoft\Co;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Rpc\Client\Annotation\Mapping\Reference;
use Swoft\Http\Message\Request;
use Swoft\Http\Message\Response;

use App\Model\Logic\UserLogic;
use Swoft\Redis\Pool;

/**
 * Class UserController
 *
 * @since 2.0
 *
 * @Controller()
 */
class UserController
{
    /**
     * @Inject()
     * @var UserLogic
     */
    protected $userLogic;

    /**
     *
     * @Inject()
     *
     * @var Pool 默认连接使用的是 redis.pool
     */
    private $redis;

    /**
     * @RequestMapping("login")
     * 
     */
    public function login(Request $request, Response $response){
        $username = $request->input('username');
        $password = $request->input('password');

        if(!$username || !$password){
            return returnError(4001, '请输入正确的信息');
        }

        $rs = $this->userLogic->login($username, $password);
        if(!$rs){
            return returnError(4004, '用户名或密码不正确');
        }
        return returnSuccess($rs, 0);
    }

    /**
     * @RequestMapping("register")
     * 
     */
    public function register(Request $request, Response $response){
        $username = $request->input('username');
        $password = $request->input('password');
        $nickname = $request->input('nickname');

        if(!$username || !$password || !$nickname){
            return returnError(4001, '请输入正确的信息');
        }

        $rs = $this->userLogic->register($nickname, $username, $password);
        if(!$rs){
            return returnError(4002, '用户名已经存在');
        }
        return returnSuccess($rs, 0);
    }

    /**
     * @RequestMapping("info")
     * 
     */
    public function info(Request $request, Response $response){
        $userId = intval($request->input('userId'));
        // var_dump($this->redis->set('aaa', 111222));
        // var_dump($this->redis->get('aaa'));

        if(!$userId){
            return returnError(4001, '请输入正确信息');
        }

        $rs = $this->userLogic->getUserInfo($userId);
        if(!$rs){
            return returnError(4004, '用户不存在');
        }
        return returnSuccess($rs, 0);
    }

    /**
     * @RequestMapping("info/update")
     * 
     */
    public function updateUserInfo(Request $request, Response $response){
        $userId = intval($request->input('userId'));
        $nickname = $request->input('nickname');
        if(!$userId || !$nickname){
            return returnError(4001, '请输入正确信息');
        }

        $introduction = $request->input('introduction', '');
        $homepage = $request->input('homepage', '');
        $rs = $this->userLogic->updateUserInfo($userId, $nickname, $introduction, $homepage);
        if(!$rs){
            return returnError(4004, '用户不存在');
        }
        return returnSuccess($rs, 0);
    }

    /**
     * @RequestMapping("top")
     * 
     */
    public function getUserTop(Request $request, Response $response){
        $rs = $this->userLogic->getUserTop();
        if(!$rs){
            return returnError(4004, '没有相关信息');
        }
        return returnSuccess($rs, 0);
    }

}
