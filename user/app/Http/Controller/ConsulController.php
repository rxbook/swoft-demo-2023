<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller;

use Exception;
use Swoft\Co;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Rpc\Client\Annotation\Mapping\Reference;
use Swoft\Limiter\Annotation\Mapping\RateLimiter;
use Swoft\Breaker\Annotation\Mapping\Breaker;

/**
 * Class ConsulController
 *
 * @since 2.0
 *
 * @Controller()
 */
class ConsulController {

    /**
     * 服务限流: 每秒钟访问1次,最大访问2次,以及回调函数limiterFallback
     * @RateLimiter(rate=1, max=2, fallback="limiterFallback")
     *
     * @RequestMapping("getList")
     *
     * @return array
     */
    public function getList(): array {
        return ['getListResult' => 'OK'];
    }

    /**
     * 限流的回调函数: 不写业务逻辑,只返回一个错误信息
     * @return array
     */
    public function limiterFallback(): array
    {
        return ['访问次数超限'];
    }

    /**
     * 服务降级
     * fallback 降级函数, 必须和 Breaker 标记的函数完全一样除了名称不一样且在同一个类里面
     * sucThreshold 连续成功3次状态切换阀门
     * failThreshold 连续失败1次状态切换阀门
     * timeout 超时时间2秒
     * retryTime 熔断器由开启状态到半开状态尝试切换时间 3秒
     *
     * @Breaker(fallback="funcFallback", sucThreshold=3, failThreshold=1, timeout=2.0, retryTime=3)
     * @RequestMapping("getInfo")
     * @return array
     * @throws Exception
     */
    public function getInfo(): array
    {
        //设定一个随机数,当随机数>=5的时候触发降级
        $times = rand(1, 10);
        echo $times;
        if($times >= 5){
            throw new Exception('Breaker exception');
        }
        echo ' -- OK' . PHP_EOL;
        return [
            'times' => $times,
            'result' => 'OK'
        ];
    }

    /**
     * 降级的回调函数
     * @return array
     */
    public function funcFallback(): array
    {
        echo ' -- error' . PHP_EOL;
        return ['error' => '服务已被降级'];
    }
    
}
