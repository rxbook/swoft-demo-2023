<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Model\Dao;
use Swoft\Bean\Annotation\Mapping\Inject;
use Swoft\Bean\Annotation\Mapping\Bean;
use App\Model\Entity\User;
use App\Model\Entity\UserInfo;
use App\Model\Entity\UserCount;

/**
 * Class UserDao
 *
 * @since 2.0
 *
 * @Bean()
 */
class UserDao
{
    /**
     * @Inject()
     * @var User
     */
    protected $user;

    /**
     * @Inject()
     * @var UserInfo
     */
    protected $userInfo;

    /**
     * @Inject()
     * @var UserCount
     */
    protected $userCount;

    public function findUserInfoByName(string $name){
        $rs = $this->user->where('name', $name)->first(['id', 'name', 'password']);
        if(!$rs){
            return [];
        }
        return [
            'id' => $rs['id'],
            'name' => $rs['name'],
            'password' => $rs['password'],
        ];
    }

    public function getUserInfo(int $userId){
        $rs = $this->userInfo->where('user_id', $userId)->first(['id', 'user_id', 'nickname', 'avatar', 'sex', 'introduction', 'homepage']);
        if(!$rs){
            return [];
        }
        return [
            'id' => $rs['id'],
            'userId' => $rs['user_id'],
            'nickname' => $rs['nickname'],
            'avatar' => $rs['avatar'],
            'sex' => $rs['sex'],
            'introduction' => $rs['introduction'],
            'homepage' => $rs['homepage']
        ];
    }

    public function saveUser(string $name, string $password){
        $arr = [
            'name' => $name,
            'password' => md5($password),
            'add_time' => time()
        ];
        $user = User::new($arr);
        $user->save();
        return $user->getId();
    }

    public function saveUserInfo(int $userId, string $nickname){
        $arr = [
            'user_id' => $userId,
            'nickname' => $nickname,
            'sex' => 0,
            'add_time' => time(),
            'update_time' => time(),
        ];
        $userInfo = UserInfo::new($arr);
        return $userInfo->save();
    }

    public function updateUserInfo(int $userId, string $nickname, string $introduction, string $homepage){
        return UserInfo::where('user_id', $userId)->update([
            'nickname' => $nickname,
            'introduction' => $introduction,
            'homepage' => $homepage,
            'update_time' => time()
        ]);
    }

    public function getUserCountList(){
        $data = [];
        $rs = $this->userCount->orderBy('fans', 'desc')->get(['user_id', 'fans', 'order_count']);
        if($rs){
            foreach($rs as $key => $val){
                array_push($data, [
                    'userId' => $val['user_id'],
                    'fans' => $val['fans'],
                    'orderCount' => $val['order_count'],
                ]);
            }
        }
        return $data;
    }

    public function getUserCount(int $userId){
        $rs = $this->userCount->where('user_id', $userId)->first(['user_id', 'fans', 'order_count', 'follow']);
        if(!$rs){
            return [];
        }
        return [
            'userId' => $rs['user_id'],
            'fans' => $rs['fans'],
            'orderCount' => $rs['order_count'],
            'follow' => $rs['follow'],
        ];
    }
}
