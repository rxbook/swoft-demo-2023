<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 * Class Count
 *
 * @since 2.0
 *
 * @Entity(table="user_count")
 */
class UserCount extends Model
{
    /**
     * @Id(incrementing=true)
     *
     * @Column(name="id", prop="id")
     * @var int|null
     */
    private $id;

    /**
     * @Column(name="user_id", prop="userId")
     * @var int|null
     */
    private $userId;

    /**
     * @Column(name="add_time", prop="addTime")
     *
     * @var int|null
     */
    private $addTime;

    /**
     * @Column()
     *
     * @var int
     */
    private $follow;

    /**
     * @Column()
     *
     * @var int
     */
    private $fans;

    /**
     * @Column(name="order_count", prop="orderCount")
     *
     * @var int
     */
    private $orderCount;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param null|int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|int
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param null|int $userId
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return null|int
     */
    public function getAddTime(): ?int
    {
        return $this->addTime;
    }

    /**
     * @param null|int $addTime
     */
    public function setAddTime(?int $addTime): void
    {
        $this->addTime = $addTime;
    }

    /**
     * @return null|int
     */
    public function getFollow(): ?int
    {
        return $this->follow;
    }

    /**
     * @param null|int $follow
     */
    public function setFollow(?int $follow): void
    {
        $this->follow = $follow;
    }

    /**
     * @return null|int
     */
    public function getFans(): ?int
    {
        return $this->fans;
    }

    /**
     * @param null|int $fans
     */
    public function setFans(?int $fans): void
    {
        $this->fans = $fans;
    }

    /**
     * @return null|int
     */
    public function getOrderCount(): ?int
    {
        return $this->orderCount;
    }

    /**
     * @param null|int $orderCount
     */
    public function setOrderCount(?int $orderCount): void
    {
        $this->orderCount = $orderCount;
    }
}
