<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link     https://swoft.org
 * @document https://swoft.org/docs
 * @contact  group@swoft.org
 * @license  https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Model\Entity;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;

/**
 *
 * Class Count
 *
 * @since 2.0
 *
 * @Entity(table="user_info")
 */
class UserInfo extends Model
{

    /**
     *
     * @Id()
     * @Column()
     * @var int|null
     */
    private $id;

    /**
     *
     *
     * @Column(name="user_id", prop="userId")
     * @var int|null
     */
    private $userId;

    /**
     *
     *
     * @Column(name="add_time", prop="addTime")
     * @var int|null
     */
    private $addTime;

    /**
     *
     *
     * @Column()
     * @var string|null
     */
    private $nickname;

    /**
     *
     *
     * @Column()
     * @var string|null
     */
    private $avatar;

    /**
     *
     * @Column()
     * @var int|null
     */
    private $sex;

    /**
     * 
     * @Column()
     * @var string|null
     */
    private $introduction;

    /**
     * 
     * @Column()
     * @var string|null
     */
    private $homepage;

    /**
     * 
     * @Column(name="update_time", prop="updateTime")
     * @var int|null
     */
    private $updateTime;

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param int|null $userId
     *
     * @return void
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @param int|null $addTime
     *
     * @return void
     */
    public function setAddTime(?int $addTime): void
    {
        $this->addTime = $addTime;
    }

    /**
     * @param string|null $nickname
     *
     * @return void
     */
    public function setNickname(?string $nickname): void
    {
        $this->nickname = $nickname;
    }

    /**
     * @param string|null $nickname
     *
     * @return void
     */
    public function setAvatar(?string $avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @param int|null $sex
     *
     * @return void
     */
    public function setSex(?int $sex): void
    {
        $this->sex = $sex;
    }

    /**
     * @param string|null $introduction
     *
     * @return void
     */
    public function setIntroduction(?string $introduction): void
    {
        $this->introduction = $introduction;
    }

    /**
     * @param string|null $homepage
     *
     * @return void
     */
    public function setHomepage(?string $homepage): void
    {
        $this->homepage = $homepage;
    }

    /**
     * @param string|null $updateTime
     *
     * @return void
     */
    public function setUpdateTime(?int $updateTime): void
    {
        $this->updateTime = $updateTime;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return int|null
     */
    public function getUpdateTime(): ?int
    {
        return $this->updateTime;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @return int|null
     */
    public function getSex(): ?int
    {
        return $this->sex;
    }

    /**
     * @return string|null
     */
    public function getIntroduction(): ?string
    {
        return $this->introduction;
    }

    /**
     * @return string|null
     */
    public function getHomepage(): ?string
    {
        return $this->homepage;
    }
}
